package be.dastudios.teamkoala.legendofthelamb.models;

import java.util.ArrayList;

public class ThingList extends ArrayList<Thing> implements java.io.Serializable{

    private String name;

    public ThingList(String aName){
        super();
        name = aName;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ThingList " +
                "name ='" + name + '\'';
    }
}