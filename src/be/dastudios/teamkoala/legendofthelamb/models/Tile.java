package be.dastudios.teamkoala.legendofthelamb.models;


public class Tile extends ThingHolder implements java.io.Serializable{

    private Tile n;
    private Tile s;
    private Tile w;
    private Tile e;
    private Tile up;
    private Tile down;

    //region Constructors

    public Tile() {
        super("New Tile", "", null, null); // init superclass
        this.n = null;
        this.s = null;
        this.w = null;
        this.e = null;
        this.up = null;
        this.down = null;
    }

    public void init(String aName, String aDescription,
                     Tile aN, Tile aS, Tile aW, Tile aE, Tile anUp, Tile aDown,
                     ThingList tl) {
        setName(aName);
        setDescription(aDescription);
        this.n = aN;
        this.s = aS;
        this.w = aW;
        this.e = aE;
        this.up = anUp;
        this.down = aDown;
        setThings(tl);
    }


    //endregion

    //region Getters and setters

    public Tile getN() {

        return n;
    }

    public void setN(Tile n) {

        this.n = n;
    }

    public Tile getS() {

        return s;
    }

    public void setS(Tile s) {

        this.s = s;
    }

    public Tile getW() {

        return w;
    }

    public void setW(Tile w) {

        this.w = w;
    }

    public Tile getE() {

        return e;
    }

    public void setE(Tile e) {

        this.e = e;
    }

    public Tile getUp() {

        return up;
    }

    public void setUp(Tile up) {

        this.up = up;
    }

    public Tile getDown() {

        return down;
    }

    public void setDown(Tile down) {

        this.down = down;
    }

    //endregion

    public String describe() {
        String roomdesc;
        String thingsdesc;

        roomdesc = String.format("%s. %s.", getName(), getDescription());
        thingsdesc = describeThings();
        if (!thingsdesc.isEmpty()) {
            roomdesc += "\nThings here:\n" + thingsdesc;
        }
        return roomdesc;
    }
}
