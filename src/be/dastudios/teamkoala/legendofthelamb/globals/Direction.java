package be.dastudios.teamkoala.legendofthelamb.globals;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    UP,
    DOWN;

    public static final int NOEXIT = -1;

}
