package be.dastudios.teamkoala.legendofthelamb.globals;

import be.dastudios.teamkoala.legendofthelamb.models.Thing;
import be.dastudios.teamkoala.legendofthelamb.models.ThingHolder;
import be.dastudios.teamkoala.legendofthelamb.models.ThingList;

public class ThingAndThingHolder implements java.io.Serializable{

    private Thing thing;
    private ThingHolder thingHolder;

    //region Constructors

    public ThingAndThingHolder(Thing aThing, ThingHolder aThingHolder) {
        thing = aThing;
        thingHolder = aThingHolder;
    }
    //endregion

    //region Getters and setters

    public Thing getThing() {
        return thing;
    }

    public void setThing(Thing aThing) {
        this.thing = aThing;
    }

    public ThingHolder getThingHolder() {
        return thingHolder;
    }

    public void setThingHolder(ThingHolder aThingHolder) {
        this.thingHolder = aThingHolder;
    }

    public ThingList getList() {
        return thingHolder.getThings();
    }
    //endregion

}
