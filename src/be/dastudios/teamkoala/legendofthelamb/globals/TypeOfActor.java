package be.dastudios.teamkoala.legendofthelamb.globals;

public enum TypeOfActor {

    HUMAN(100),
    DWARFMALE(20),
    DWARFFEMALE(20),
    ELFMALE(50),
    ELFFMALE(50),
    ROGUEMALE(10),
    ROGUEFEMALE(10);

    private final int value;

    TypeOfActor(int value) {

        this.value = value;
    }

    public int getValue() {

        return value;
    }
}