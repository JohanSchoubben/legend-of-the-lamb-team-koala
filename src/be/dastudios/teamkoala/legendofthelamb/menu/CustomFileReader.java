package be.dastudios.teamkoala.legendofthelamb.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CustomFileReader {
    private Path path;
    private static boolean checkIfExist;
    private static boolean checkIfAccessible;


    public CustomFileReader() {
    }

    public static void readTextFile(Path path) {
        Path pathToRead = Path.of(String.valueOf(path));
        checkIfExist = Files.exists(pathToRead);
        checkIfAccessible = Files.isReadable(pathToRead) & Files.isWritable(pathToRead) &
                            Files.isRegularFile(pathToRead) & Files.isExecutable(pathToRead);
        if (checkIfExist && checkIfAccessible) {
            try (BufferedReader bufferedReader = Files.newBufferedReader(pathToRead)) {
                String lineOfText = null;
                while ((lineOfText = bufferedReader.readLine()) != null) {
                    System.out.println(lineOfText);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
            System.out.println("File does not exist or cannot be accessed");
        }
    }
}