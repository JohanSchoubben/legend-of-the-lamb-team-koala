package be.dastudios.teamkoala.legendofthelamb.utilities.dice;

import java.util.Random;

/**
 * Generates die or dice of type dn from 1 to 'n' included
 *
 * @param 'n' sets the upper bound limit for the random generator
 */

public class Dice {
    private final Random random = new Random();
    private int n;
    private int numberOfDice;

    public Dice() {};

    public int rollDie(int n) {
        return random.nextInt(n) + 1;
    }

    public int rollMultipleDice(int numberOfDice, int n) {
        int sum = 0;
        for (int i = 0; i < numberOfDice; i++) {
            sum += rollDie(n);
        }
        return (int) sum / numberOfDice;
    }
}