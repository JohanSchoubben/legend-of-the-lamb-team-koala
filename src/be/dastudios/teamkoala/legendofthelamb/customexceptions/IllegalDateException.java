package be.dastudios.teamkoala.legendofthelamb.customexceptions;

/**
 * Class for custom exception handling for date input format validation
 */
public class IllegalDateException extends Throwable {

    public IllegalDateException(String s) {
        super(s);
    }
}
