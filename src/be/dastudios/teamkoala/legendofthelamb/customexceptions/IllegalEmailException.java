package be.dastudios.teamkoala.legendofthelamb.customexceptions;

/**
 * Class for handling exceptions for Email address input format validation
 */
public class IllegalEmailException extends Throwable {

    public IllegalEmailException(String s) {

        super(s);

    }
}
