package be.dastudios.teamkoala.legendofthelamb.customexceptions;

/**
 * Class for handling exceptions for time input format validation
 */
public class IllegalTimeException extends Throwable {

    public IllegalTimeException(String s) {

        super(s);


    }
}
