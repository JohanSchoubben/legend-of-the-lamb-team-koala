package be.dastudios.teamkoala.legendofthelamb.game;

import be.dastudios.teamkoala.legendofthelamb.globals.GrammarType;
import be.dastudios.teamkoala.legendofthelamb.utilities.fileutility.FileUtility;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static be.dastudios.teamkoala.legendofthelamb.game.AdventureGame.game;

public class ParserUtility {

    static HashMap<String, GrammarType> vocabulary = new HashMap<>();

    public static final String BASE_PATH = "src/be/dastudios/teamkoala/legendofthelamb/files/";

    static final String FILES_GAMECONTROLS = BASE_PATH + "gamecontrols/";

    static void initVocab() {



        Path pathToVerbsFile = Path.of(FILES_GAMECONTROLS + "commands.txt");
        boolean exists = FileUtility.checkExistsPath(pathToVerbsFile);
        fillStringDataFromFile(pathToVerbsFile, exists, GrammarType.VERB);


        Path pathToNounsFile = Path.of(FILES_GAMECONTROLS + "objects.txt");
        exists = FileUtility.checkExistsPath(pathToNounsFile);
        fillStringDataFromFile(pathToNounsFile, exists, GrammarType.NOUN);

        Path pathToAdjectivesFile = Path.of(FILES_GAMECONTROLS + "adjectives.txt");
        exists = FileUtility.checkExistsPath(pathToAdjectivesFile);
        fillStringDataFromFile(pathToAdjectivesFile, exists, GrammarType.ADJECTIVE);

        Path pathToPrepositionFile = Path.of(FILES_GAMECONTROLS + "preposition.txt");
        exists = FileUtility.checkExistsPath(pathToPrepositionFile);
        fillStringDataFromFile(pathToPrepositionFile, exists, GrammarType.PREPOSITION);

        Path pathToArticleFile = Path.of(FILES_GAMECONTROLS + "article.txt");
        exists = FileUtility.checkExistsPath(pathToArticleFile);
        fillStringDataFromFile(pathToArticleFile, exists, GrammarType.ADJECTIVE);

    }

    private static void fillStringDataFromFile(Path path, boolean exists, GrammarType type) {

        if (exists) {
            String[] data = FileUtility.getItemsFromFile(path);
            fillVocabMap(data, type);
        }
    }


    private static void fillVocabMap(String[] data, GrammarType type) {

        for (String word : data) {
            vocabulary.put(word, type);
        }
    }

    public static HashMap<String, GrammarType> getVocabulary() {

        return vocabulary;
    }

    static String processVerbNounPrepositionNoun(List<WordAndType> command) {

        WordAndType wt = command.get(0);
        WordAndType wt2 = command.get(1);
        WordAndType wt3 = command.get(2);
        WordAndType wt4 = command.get(3);
        String msg = "";

        if ((wt.getWordType() != GrammarType.VERB) || (wt3.getWordType() != GrammarType.PREPOSITION)) {
            msg = "Can't do this because I don't understand ho to '" + wt.getWord() + " something " + wt3.getWord() + "' something!";
        }
        else if (wt2.getWordType() != GrammarType.NOUN) {
            msg = "Can't do this because '" + wt2.getWord() + "' is not an object!\r\n";
        }
        else if (wt4.getWordType() != GrammarType.NOUN) {
            msg = "Can't do this because '" + wt4.getWord() + "' is not an object!\r\n";
        }
        else {
            switch (wt.getWord() + wt3.getWord()) {
                case "putin":
                case "putinto":
                    msg = game.putObInContainer(wt2.getWord(), wt4.getWord());
                    break;
                default:
                    msg = "I don't know how to " + wt.getWord() + " " + wt2.getWord() + " " + wt3.getWord() + " " + wt4.getWord() + "!";
                    break;
            }
        }
        return msg;
    }

    static String processVerbPrepositionNoun(List<WordAndType> command) {
        // "look at" is the only implemented command of this type
        WordAndType wt = command.get(0);
        WordAndType wt2 = command.get(1);
        WordAndType wt3 = command.get(2);
        String msg = "";

        if ((wt.getWordType() != GrammarType.VERB) || (wt2.getWordType() != GrammarType.PREPOSITION)) {
            msg = "Can't do this because I don't understand '" + wt.getWord() + " " + wt2.getWord() + "' !";
        }
        else if (wt3.getWordType() != GrammarType.NOUN) {
            msg = "Can't do this because '" + wt3.getWord() + "' is not an object!\r\n";
        }
        else {
            switch (wt.getWord() + wt2.getWord()) {
                case "lookat":
                    msg = game.lookAtOb(wt3.getWord());
                    break;
                case "lookin":
                    msg = game.lookInOb(wt3.getWord());
                    break;
                default:
                    msg = "I don't know how to " + wt.getWord() + " " + wt2.getWord() + " " + wt3.getWord() + "!";
                    break;
            }
        }
        return msg;
    }

    static String processVerbNoun(List<WordAndType> command) {

        WordAndType wt = command.get(0);
        WordAndType wt2 = command.get(1);
        String msg = "";

        if (wt.getWordType() != GrammarType.VERB) {
            msg = "Can't do this because '" + wt.getWord() + "' is not a command!";
        }
        else if (wt2.getWordType() != GrammarType.NOUN) {
            msg = "Can't do this because '" + wt2.getWord() + "' is not an object!";
        }
        else {
            switch (wt.getWord()) {
                case "take":
                case "get":
                    msg = game.takeOb(wt2.getWord());
                    break;
                case "drop":
                    msg = game.dropOb(wt2.getWord());
                    break;
                case "open":
                    msg = game.openOb(wt2.getWord());
                    break;
                case "close":
                    msg = game.closeOb(wt2.getWord());
                    break;
                default:
                    msg += " (not yet implemented)";
                    break;
            }
        }
        return msg;
    }

    static String processVerb(List<WordAndType> command) {

        WordAndType wt = command.get(0);
        String msg = "";

        if (wt.getWordType() != GrammarType.VERB) {
            msg = "Can't do this because '" + wt.getWord() + "' is not a command!";
        }
        else {
            switch (wt.getWord()) {
                case "n":
                    game.goN();
                    break;
                case "s":
                    game.goS();
                    break;
                case "w":
                    game.goW();
                    break;
                case "e":
                    game.goE();
                    break;
                case "up":
                    game.goUp();
                    break;
                case "down":
                    game.goDown();
                    break;
                case "l":
                case "look":
                    game.look();
                    break;
                case "inventory":
                case "i":
                    game.showInventory();
                    break;
                case "test":
                    game.test();
                    break;
                default:
                    msg = wt.getWord() + " (not yet implemented)";
                    break;
            }
        }
        return msg;
    }

    static String processCommand(List<WordAndType> command) {

        String s = "";

        if (command.size() == 0) {
            s = "You must write a command!";
        }
        else if (command.size() > 4) {
            s = "That command is too long!";
        }
        else {
            switch (command.size()) {
                case 1:
                    s = processVerb(command);
                    break;
                case 2:
                    s = processVerbNoun(command);
                    break;
                case 3:
                    s = processVerbPrepositionNoun(command);
                    break;
                case 4:
                    s = processVerbNounPrepositionNoun(command);
                    break;
                default:
                    s = "Unable to process command";
                    break;
            }
        }
        return s;
    }

    static String parseCommand(List<String> wordlist) {

        List<WordAndType> command = new ArrayList<>();
        GrammarType wordtype;
        String errmsg = "";
        String msg;

        for (String k : wordlist) {
            if (vocabulary.containsKey(k)) {
                wordtype = vocabulary.get(k);
                if (wordtype == GrammarType.ARTICLE) {       // ignore articles
                }
                else {
                    command.add(new WordAndType(k, wordtype));
                }
            }
            else { // if word not found in vocab
                command.add(new WordAndType(k, GrammarType.ERROR));
                errmsg = "Sorry, I don't understand '" + k + "'";
            }
        }
        if (!errmsg.isEmpty()) {
            msg = errmsg;
        }
        else {
            msg = processCommand(command);
        }
        return msg;
    }

    static List<String> wordList(String input) {

        String delims = "[ \t,.:;?!\"']+";
        List<String> strlist = new ArrayList<>();
        String[] words = input.split(delims);

        for (String word : words) {
            strlist.add(word);
        }
        return strlist;
    }
}
