package be.dastudios.teamkoala.legendofthelamb.game;

import be.dastudios.teamkoala.legendofthelamb.globals.Direction;
import be.dastudios.teamkoala.legendofthelamb.globals.TypeOfActor;
import be.dastudios.teamkoala.legendofthelamb.models.*;
import be.dastudios.teamkoala.legendofthelamb.utilities.fileutility.FileUtility;
import be.dastudios.teamkoala.legendofthelamb.utilities.fileutility.MenuUtility;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game implements java.io.Serializable {

    public static final String BASE_PATH = "src/be/dastudios/teamkoala/legendofthelamb/files/";
    private static final String FILE_PATH_TO_DESCRIPTION = BASE_PATH + "gamecontrols/objectsdescription.txt";

    private ArrayList<Tile> map;
    private Actor player;

    Tile exitTile = null;
    private static int pointsCollected = 0;

    //region Constructor

    public Game() {

//        ParserUtility.initVocab();
        initGame();

    }

    //endregion

    private void initGame() {

        this.map = new ArrayList<Tile>();

        ThingList dwarfTileList1 = new ThingList("dwarfTileList1");
        ThingList dwarfTileList2 = new ThingList("dwarfTileList2");
        ThingList dwarfTileList3 = new ThingList("dwarfTileList3");
        ThingList dwarfTileList4 = new ThingList("dwarfTileList4");

        ThingList forestList1 = new ThingList("forestList1");
        ThingList forestList2 = new ThingList("forestList2");
        ThingList forestList3 = new ThingList("forestList3");

        ThingList caveList1 = new ThingList("caveList1");
        ThingList caveList2 = new ThingList("caveList2");

        ThingList dungeonList1 = new ThingList("dungeonList1");
        ThingList dungeonList2 = new ThingList("dungeonList2");

        ThingList shedList = new ThingList("shedList");

        ThingList exitList = new ThingList("exitList");

        ThingList playerlist = new ThingList("playerlist");

        Tile dwarfTile1 = new Tile();
        Tile dwarfTile2 = new Tile();
        Tile dwarfTile3 = new Tile();
        Tile dwarfTile4 = new Tile();
        Tile forestTile1 = new Tile();
        Tile forestTile2 = new Tile();
        Tile forestTile3 = new Tile();
        Tile caveTile1 = new Tile();
        Tile caveTile2 = new Tile();
        Tile shedTile = new Tile();
        Tile dungeonTile1 = new Tile();
        Tile dungeonTile2 = new Tile();

        Tile exitTile = new Tile();


        fillListWithTreasure(exitList, exitTile);

//      Tile(                   name,                       description,                        N,        S,            W,        E,        [Up],       [Down]      list)
        fillListWithTreasure(dwarfTileList1, dwarfTile1);
        dwarfTile1.init("Dwarf Tile1", "A dark room that smells of dwarfs", null, caveTile1, null, forestTile2, null, null, dwarfTileList1);

        fillListWithTreasure(dwarfTileList2, dwarfTile2);
        dwarfTile2.init("Dwarf Tile2", "Another dark room that smells of dwarfs", dungeonTile1, null, forestTile3, exitTile, null, null, dwarfTileList2);

        fillListWithTreasure(dwarfTileList3, dwarfTile3);
        dwarfTile3.init("Dwarf Tile3", "Anaddic that crawls with dwarfs", null, dwarfTile4, forestTile2, null, null, null, dwarfTileList3);

        fillListWithTreasure(dwarfTileList4, dwarfTile4);
        dwarfTile4.init("Dwarf Tile4", "A dank room that smells of dwarfs", dwarfTile3, dungeonTile1, null, null, null, null, dwarfTileList4);

        fillListWithTreasure(forestList1, forestTile1);
        forestTile1.init("Forest 1", "A leafy woodland", null, forestTile3, caveTile2, dungeonTile1, null, null, forestList1);

        fillListWithTreasure(forestList2, forestTile2);
        forestTile2.init("Forest 2", "A leafy woodland", null, null, dwarfTile1, dwarfTile3, null, null, forestList2);

        fillListWithTreasure(forestList3, forestTile3);
        forestTile3.init("Forest 3", "A leafy woodland", forestTile1, null, dungeonTile2, dwarfTile2, null, null, forestList3);

        fillListWithTreasure(caveList1, caveTile1);
        caveTile1.init("Cave 1", "A dismal cave with walls covered in luminous moss", dwarfTile1, caveTile2, null, shedTile, null, null, caveList1);

        fillListWithTreasure(caveList2, caveTile2);
        caveTile2.init("Cave 2", "A dismal cave with walls covered in luminous moss", caveTile1, dungeonTile2, null, forestTile1, null, null, caveList2);

        fillListWithTreasure(shedList, shedTile);
        shedTile.init("Shed", "An old, wooden shed", null, null, caveTile1, null, null, null, shedList);

        fillListWithTreasure(dungeonList1, dungeonTile1);
        dungeonTile1.init("Dungeon 1", "A nasty, dark cell", forestTile1, dwarfTile2, forestTile1, null, caveTile2, null, dungeonList1);

        fillListWithTreasure(dungeonList2, dungeonTile2);
        dungeonTile2.init("Dungeon 2", "A nasty, dark cell", caveTile2, null, null, forestTile3, null, null, dungeonList2);

        exitTile.init("exit", "You've made it!!!", null, null, dwarfTile2, null, null, null, exitList);

        map.add(dwarfTile1);
        map.add(dwarfTile2);
        map.add(dwarfTile3);
        map.add(dwarfTile4);
        map.add(forestTile1);
        map.add(forestTile2);
        map.add(forestTile3);
        map.add(caveTile1);
        map.add(caveTile2);
        map.add(shedTile);
        map.add(dungeonTile1);
        map.add(dungeonTile2);

        map.add(exitTile);

        String[] avatarChoices = FileUtility.getNamesFromEnum(TypeOfActor.class);
        int avatar = FileUtility.askForChoice("Choose a avatar", avatarChoices);

        player = new Actor(FileUtility.ask("Enter your character's name"),
                avatarChoices[avatar],
                playerlist, dwarfTile1);
        playerlist.add(player);

    }

    private void fillListWithTreasure(ThingList tileList, Tile tile) {

        Random rndObjectPicker = new Random();

        String[] strTreasures = FileUtility.getItemsFromFile(Path.of(FILE_PATH_TO_DESCRIPTION));

        int maxTreasures = rndObjectPicker.nextInt((strTreasures.length / 15)) + 1;
        ArrayList<String> toAddTreasures = new ArrayList<>();
        for (int i = 0; i < strTreasures.length; i++) {
            if (i == maxTreasures) {
                break;
            }
            int rnd = rndObjectPicker.nextInt(strTreasures.length);
            toAddTreasures.add(i, strTreasures[rnd]);
        }

        for (int i = 0; i < strTreasures.length; i++) {

            String entry = strTreasures[i];
            String[] line = entry.split(",");

            String[] toSearchObject = findObject(line, toAddTreasures);
            if (toSearchObject != null) {

                String treasure = toSearchObject[0];
                String description = toSearchObject[1];
                boolean canTake = Integer.parseInt(toSearchObject[2]) == 0 ? false : true;
                boolean canMove = Integer.parseInt(toSearchObject[3]) == 0 ? false : true;
                boolean isContainer = Integer.parseInt(toSearchObject[4]) == 0 ? false : true;
                if (canTake && canMove && isContainer) {//take and move
                    tileList.add(new ContainerThing(treasure, description, canTake, canMove, true, true, new ThingList(treasure + "List"), tile));
                    return;
                }
                else if ((!canTake ||canMove) && (!isContainer)) {// no take no move
                    tileList.add(new Thing(treasure, description, canTake, canMove, tile));
                    return;
                }
                else  {// no take, can move
                    tileList.add(new Treasure(treasure, description, 1, tile));
                    return;
                }
            }
        }
    }

    private String[] findObject(String[] line, ArrayList<String> toAddTreasures) {

        String[] foundObject = null;

        String strObject = line[0];

        for (int i = 0; i < toAddTreasures.size(); i++) {
            String s = toAddTreasures.get(i);
            String[] search = s.split(",");

            for (int j = 0; j < search.length; j++) {
                if (search[0].equals(strObject)) {
                    return line;
                }
            }
        }

        return foundObject;

    }

    public String runCommand(String inputstr) {

        List<String> wordlist;
        String s = "ok";
        String lowstr = inputstr.trim().toLowerCase();

        if (!lowstr.equals("q")) {
            if (lowstr.equals("")) {
                s = "You must enter a command";
            }
            else {
                wordlist = ParserUtility.wordList(lowstr);
                s = ParserUtility.parseCommand(wordlist);
            }
        }
        return s;
    }

    //region Getters and setters

    public Tile getExitTile() {

        return exitTile;
    }

    private ArrayList getMap() {

        return map;
    }

    private void setMap(ArrayList aMap) {

        map = aMap;
    }

    public Actor getPlayer() {

        return player;
    }

    private void setPlayer(Actor aPlayer) {

        player = aPlayer;
    }
    //endregion

    //region Object manipulation methods

    public String putObInContainer(String obname, String containername) {

        return player.putInto(obname, containername);
    }

    public String openOb(String obname) {

        return player.openOb(obname);
    }

    public String closeOb(String obname) {

        return player.closeOb(obname);
    }

    String takeOb(String obname) {

        String retStr = "";
        retStr = player.take(obname);
        return retStr;
    }

    String dropOb(String obname) {

        String retStr = "";
        retStr = player.drop(obname);
        return retStr;
    }
    //endregion

    //region Move methods

    private void moveActorTo(Actor p, Tile aTile) {

        p.setLocation(aTile);
    }

    void movePlayerTo(Direction dir) {

        if (!player.moveTo(dir)) {
            showStr("No Exit!");
        }
        else if (player.getLocation().getName() == "exit") {
            System.out.println("You've won, congratulations");
            System.out.println();
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            int points = scoring(player);

            System.out.println("You've scored " + points);
            System.out.println("Not bad, but you can do better...");
            System.out.println();
            System.out.println("Going back to the main menu...");

            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                AdventureGame.main(null);
            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }
        else {
            look();
        }

    }

    private int scoring(Actor player) {

        int pointsCollected = 0;

        String inventory = player.inventory();

        String[] listOfThingsCollected = inventory.split(",");

        String tempCleanup = listOfThingsCollected[0];
        String tempCleanup2 = tempCleanup.substring(13);
        listOfThingsCollected[0] = tempCleanup2;

        for (String item : listOfThingsCollected) {
            switch (item) {
                case "sword":
                case "spell":
                case "parchment":
                case "dagger":
                case "potion":
                    pointsCollected += 50;
                    break;
                case "bow":
                case "longbow":
                case "axe":
                case "javelin":
                case "wand":
                case "trophy":
                    pointsCollected += 40;
                    break;
                case "book":
                case "scroll":
                case "key":
                case "kite":
                    pointsCollected += 30;
                    break;
                default:
                    pointsCollected += 10;
            }
        }

        return pointsCollected;
    }
    //endregion

    //region Navigation methods

    void goN() {

        movePlayerTo(Direction.NORTH);
    }

    void goS() {

        movePlayerTo(Direction.SOUTH);
    }

    void goW() {

        movePlayerTo(Direction.WEST);
    }

    void goE() {

        movePlayerTo(Direction.EAST);
    }

    void goUp() {

        movePlayerTo(Direction.UP);
    }

    void goDown() {

        movePlayerTo(Direction.DOWN);
    }
    //endregion

    //region Look methods

    void look() {

        showStr("You are in the " + player.describeLocation());
    }


    String lookAtOb(String obname) {

        return player.lookAt(obname);
    }

    String lookInOb(String obname) {

        return player.lookIn(obname);
    }
    //endregion

    //region Show methods

    void showStr(String s) {

        if (s.endsWith("\n")) {
            s = s.substring(0, s.length() - 1);

        }
        if (!s.isEmpty()) {
            System.out.println(s);
        }
    }

    void showInventory() {

        showStr(player.inventory());

    }

    public void showIntro() {

        String s;

        s = "You have fallen down a rabbit hole and arrived in\n"
                + "an underground cavern that smells strongly of troll.\n"
                + "Where do you want to go?\n"
                + "Enter: n, s, w, e\n"
                + "or q to quit.";
        showStr(s);
        System.out.println(MenuUtility.thickLine());
        look();
    }

    void showVisibleThings(ThingList tl) {

        for (Thing t : tl) {
            showStr(t.getName());
            if ((t instanceof ContainerThing) && ((ContainerThing) t).isOpen()) {
                showVisibleThings(((ContainerThing) t).getThings());
            }
        }
    }

    void showThingsInRoom() { // shows only 'top-level' objects in list
        ThingList tl = player.getLocation().getThings();
        for (Thing t : tl) {
            showStr(t.getName());
        }
    }

    void showTest(String s) {

        showStr("> " + s);
        showStr(runCommand(s));

    }
    //endregion

    //region Tests

    void test() {
        // utility method to let me try out bits of code while developing the game
        // Here I can enter commands, just as the player would when
        // playing the game. the showTest() method shows the command and the
        // game's reply...
        showStr("---BEGIN TEST---");
        showTest("get carrot");
        showTest("get bowl");
        showTest("put carrot in bowl");
        showTest("put bowl in sack");
        showTest("drop sack");
        /*
         * showTest("get carrot");
         * showTest("put carrot in bowl");
         * showTest("put carrot in bowl");
         * showTest("get sack");
         * showTest("put sack in carrot");
         * showTest("put sack in sack");
         * showTest("close sack");
         * showTest("put bowl in sack");
         * showTest("open sack");
         * showTest("put bowl in sack");
         * showTest("i");
         * showTest("put sack in bowl");
         * showTest("i");
         */
        System.out.println("showThingsInRoom()");
        showThingsInRoom(); // this works ok when no objects are in containers
        System.out.println("showVisibleThings()");
        showVisibleThings(player.getLocation().getThings()); // to show things inside other things
        showStr("---END TEST---");
    }
    // Test..... END
    //endregion

}