package be.dastudios.teamkoala.legendofthelamb.game;

import be.dastudios.teamkoala.legendofthelamb.globals.GrammarType;

public class WordAndType {

    /*
     * This class is used as a matching Key-Value pair to store items in the vocabularyMap
     */

    private String word;
    private GrammarType wordType;

    //region Constructors

    public WordAndType(String word, GrammarType wordType) {

        this.word = word;
        this.wordType = wordType;
    }
    //endregion

    //region Getters and setters

    public String getWord() {

        return word;
    }

    public void setWord(String word) {

        this.word = word;
    }

    public GrammarType getWordType() {

        return wordType;
    }

    public void setWordType(GrammarType wordType) {

        this.wordType = wordType;
    }
    //endregion


}
