package be.dastudios.teamkoala.legendofthelamb.game;

import be.dastudios.teamkoala.legendofthelamb.menu.DisplayMenu;
import be.dastudios.teamkoala.legendofthelamb.utilities.fileutility.FileUtility;
import be.dastudios.teamkoala.legendofthelamb.utilities.fileutility.MenuUtility;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;


public class AdventureGame {

    static Game game;

    static BufferedReader in;

    static final String FILE_EXT = ".dat";
    public static final String BASE_PATH = "src/be/dastudios/teamkoala/legendofthelamb/files/";
    static final String FILES_PLAYERDATA = BASE_PATH + "playerdata/";
    static final String MAIN_MENU_PATH = BASE_PATH+"menu/mainmenu.txt";

    static String[] mainMenuChoices = FileUtility.getItemsFromFile(Path.of(MAIN_MENU_PATH));

    public static void main(String[] args) throws IOException {


        displayHeaderInfo();
        System.out.println();

        Path pathMainMenu = Path.of(MAIN_MENU_PATH);
        String[] mainMenuChoices = FileUtility.getItemsFromFile(pathMainMenu);
        displayMainMenu(mainMenuChoices);
        playGame();
    }

    private static void displayMainMenu(String[] mainMenuChoices) {

        try {

            int choice;
            do {
                choice = FileUtility.askForChoice("Make a choice", mainMenuChoices);
                menuChoiceActions(choice);
                if(choice == 1){
                    gameLoop("");
                }
            } while (choice != 5 );
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void playGame() throws IOException {

        String input;
        String output = "";
        ParserUtility.initVocab();
        game = new Game();
        in = new BufferedReader(new InputStreamReader(System.in));
        gameLoop(output);
    }

    private static void gameLoop(String output) throws IOException {

        ParserUtility.initVocab();
        String input;
        output = "";
        game.showIntro();
        do {
            System.out.print("> ");
            input = in.readLine();
            switch (input) {
                case "save":
                    saveGame();
                    output = "";
                    break;
                case "load":
                    loadGame();
                    gameLoop("");
                    break;
                default:
                    output = game.runCommand(input);
                    break;
            }
            if (!output.trim().isEmpty()) {
                game.showStr(output);
            }
        } while (!"q".equals(input));
    }

    public static void saveGame() {

        String fn;
        boolean doSave;
        String name = game.getPlayer().getName();

        doSave = true;
        fn = FileUtility.makeFileName(name);
        Path savedGame = Path.of(FILES_PLAYERDATA).resolve(fn);
        if (fn.isEmpty()) {
            doSave = false;
        }
        else if (FileUtility.fileExists(fn)) {
            if (!FileUtility.overwriteFile(fn)) {
                doSave = false;
            }
        }
        if (doSave) {
            try {
                boolean exist = FileUtility.checkExistsPath(savedGame);
                if (exist) {
                    FileUtility.saveToDestination(savedGame, fn, FILE_EXT,game);
                }
                else {
                    Path folderPath = Path.of(FILES_PLAYERDATA + name);
                    boolean existFolder = FileUtility.checkExistsPath(folderPath);
                    Path newFolder = Path.of(FILES_PLAYERDATA, name);
                    if (existFolder) {
                        try {
                            FileUtility.saveToDestination(newFolder, fn,FILE_EXT,game);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    else {

                        try {
                            Files.createDirectory(newFolder);
                            FileUtility.saveToDestination(newFolder, fn,FILE_EXT,game);
                        }
                        catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            }
            catch (Exception e) {
                System.out.print("Serialization Error! Can't save data.\n"
                        + e.getClass() + ": " + e.getMessage());
            }
        }
        else {
            System.out.println("File not saved");
        }
    }

    public static void loadGame() {

        String playerName = FileUtility.ask("Enter your name");
        Path pathToLoad = null;
        Path pathToPlayerMap = Path.of(FILES_PLAYERDATA + playerName);

        if (FileUtility.checkExistsPath(pathToPlayerMap)) {
            File[] listOfFiles = FileUtility.getFileList(pathToPlayerMap,FILE_EXT);

            if(listOfFiles.length == 0){
                System.out.println("There are no files to load. Returning to main menu");
                System.out.println(MenuUtility.thickLine());
                System.out.println();
                try {
                    Thread.sleep(1500);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                displayMainMenu(mainMenuChoices);

            }

            String[] listOfFileNames = FileUtility.convertFilesListInStringList(listOfFiles);
            int choice = FileUtility.askForChoice("Which file do you want to load", listOfFileNames);

            pathToLoad = Path.of(listOfFiles[choice].toURI());

            String fileToLoad = pathToLoad.toString();

            if (fileToLoad.isEmpty()) {
                System.out.println("File loading failed");
            }
            else {
                try {
                    boolean exist = FileUtility.checkExistsPath(pathToLoad);
                    if (exist) {
                        FileInputStream fis = new FileInputStream(String.valueOf(pathToLoad));
                        ObjectInputStream ois = new ObjectInputStream(fis);

                        game = (Game) ois.readObject();

                        ois.close();
                        System.out.print("\n---Game loaded---\n");
                    }
                    else {
                        System.out.println("File not found");
                    }
                }
                catch (Exception e) {
                    System.out.print("Serialization Error! Can't load data.\n");
                    System.out.print(e.getClass() + ": " + e.getMessage());
                }
            }
        }
        else {
            System.out.println("File does not exist. Returning to main menu!");

        }
    }

    private static void menuChoiceActions(int choice) throws IOException {

        in = new BufferedReader(new InputStreamReader(System.in));

        boolean flag = true;

        while (flag) {

            switch (choice) {
                case 0:
                    playGame();
                    flag = false;
                    break;
                case 1:
                    System.out.println("Game is being loaded...");
                    loadGame();
                    flag = false;
                    break;
                case 2:
                    System.out.println("Clearing list of Player ...");
                    clearPlayerList();
                    flag = false;
                    break;
                case 3:
                    System.out.println("List of commands");
                    System.out.println(MenuUtility.thinLine());
                    showListCommands();
                    flag = false;
                    break;
                case 4:
                    System.out.println("Game settings under construction");
                    flag = false;
                    break;
                case 5:
                    System.out.println("Game is being saved...");
                    saveGame();
                    flag = false;
                    System.exit(0);
                default:
                    System.out.println("You made the wrong choice. Please try again");
                    flag = true;
            }
        }
    }

    private static void clearPlayerList() {

        String playerName = FileUtility.ask("Enter your name");
        Path pathToPlayerMap = Path.of(FILES_PLAYERDATA + playerName);

        boolean areDeleted = false;
        try {
            areDeleted = FileUtility.deletefilesDirectory(pathToPlayerMap,FILE_EXT);
            if (areDeleted) {
                System.out.println("All files are deleted.");
                System.out.println();
            }
        }
        catch (UncheckedIOException e) {
            System.out.println("Something went wrong. Files in directory are not deleted.");
        }



    }

    /**
     * Method for displaying a list of commands
     */
    private static void showListCommands() {

        Path pathCommandList = Path.of(BASE_PATH + "gamecontrols/commands.txt");

        String[] commandList = FileUtility.getItemsFromFile(pathCommandList);

        for (int i = 0; i < commandList.length; i++) {
            if ((i != 0) && (i % 3 == 0)) {
                System.out.printf("%n");
            }
            System.out.printf("%-15.15s", commandList[i]);

        }
        System.out.println();
        System.out.println(MenuUtility.thinLine());
        System.out.println();
    }

    /**
     * Method for displaying the header info
     */
    private static void displayHeaderInfo() {

        Path pathHeader = Path.of(BASE_PATH + "menu/welcome.txt");
        DisplayMenu.displayMenuItem(pathHeader);

        Path pathGreeting = Path.of(BASE_PATH + "menu/mainmenuheader.txt");
        DisplayMenu.displayMenuItem(pathGreeting);
    }
}